import React from 'react'

import { 
    StyleSheet, 
    TouchableOpacity, 
    Text, 
    View 
} from 'react-native'

import IconEntypo from 'react-native-vector-icons/Entypo';
import IconEvilicons from 'react-native-vector-icons/EvilIcons';
import LinearGradient from 'react-native-linear-gradient'
import { Actions } from 'react-native-router-flux';

import { 
    Container, 
    Content, 
    Item, 
    Input 
} from 'native-base'
class CFormLogin extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Container style={styles.bgStyleCustomHidden}>
            <Content>
              <Item style={[styles.bgStyleCustomShow, styles.paddings]}>
                  <View style={styles.borderOfIcon}>
                      <IconEntypo size={20} name='user' color="#aaaaaa"/>
                  </View>
                  <Input placeholder='Endereço de e-mail' style={{height: 60}}/>
              </Item>
              <Item style={styles.bgStyleCustomShow}>
                  <IconEvilicons size={40} name='lock' color="#aaaaaa" />
                  <Input placeholder='Senha' style={{height: 60}}/>
              </Item>
              <LinearGradient
                      colors={['#1283f6', '#8811d3']}
                      start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                      style={styles.containerButton}
                      >
                  <TouchableOpacity 
                      onPress={() => Actions.organizations()}>
                      <Text style={styles.buttonText}>ENTRAR</Text>
                  </TouchableOpacity>
              </LinearGradient>
              <View style={styles.recoveryPassword}>
                  <Text style={{color: '#fff', alignItems: 'center', alignSelf: 'center'}}>ESQUECEU SUA SENHA?</Text>
              </View>
            </Content>
            </Container>
        )
    }
}
    

const styles = StyleSheet.create({
  bgStyleCustomHidden: {
      backgroundColor: 'transparent',
  },
  bgStyleCustomShow: {
      backgroundColor: '#ffffff',
  },
  paddings: {
      paddingLeft: 7
  },
  buttonText: {
      color: '#FFF',
      padding: 15,
  },
  containerButton: {
      width: 170,
      height: 50,
      alignItems: 'center',
      alignSelf: 'flex-end',
  },
  recoveryPassword: {
      height: 100,
      justifyContent: 'center'
  },
})

export { CFormLogin }