import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text 
} from 'react-native'

import { 
    Container, 
    Content, 
    Input, 
    List, 
    ListItem 
} from 'native-base'

class CFormSignup extends Component {
    constructor(props) {
        super(props)
    }    
  render() {
    return (
      <Container style={styles.bgStyleCustomHidden}>
      <Content>
        <List>
          <ListItem itemDivider>
            <Text>A</Text>
          </ListItem>
          <ListItem>

              <Input placeholder="Nome"/>
          </ListItem>
        </List>
      </Content>
    </Container>
    )
  }
}

const styles = StyleSheet.create({
  bgStyleCustomHidden: {
      backgroundColor: 'transparent',
  },
  bgStyleCustomShow: {
      backgroundColor: '#ffffff',
  },
  paddings: {
      paddingLeft: 10
  },
  buttonText: {
      color: '#FFF',
      padding: 15,
  },
  containerButton: {
      width: 170,
      height: 50,
      alignItems: 'center',
      alignSelf: 'flex-end',
  },
  recoveryPassword: {
      height: 100,
      justifyContent: 'center'
  },
});

export { CFormSignup };
