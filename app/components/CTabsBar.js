import React from 'react'
import { StyleSheet } from 'react-native'

import TabNavigator from 'react-native-tab-navigator'

import { 
    CFormLogin, 
    CFormSignup 
} from './index'

class CTabsBar extends React.Component {
  state = {
    selectedTab: 'login',
  }
  render(){
      return (
        <TabNavigator 
            style={{ height: '100%' }} 
            sceneStyle={styles.removeStyleOfTabs} 
            tabBarStyle={styles.removeStyleOfTabBar}>
            <TabNavigator.Item
                selected={this.state.selectedTab === 'login'}
                title="ENTRAR"
                onPress={() => this.setState({ selectedTab: 'login' })}
                selectedTitleStyle={{color: '#ffffff', fontSize: 20, opacity: 1}}
                titleStyle={{color: '#ffffff', fontSize: 20, opacity: 0.5}}>
                <CFormLogin />
            </TabNavigator.Item>
            <TabNavigator.Item
                selected={this.state.selectedTab === 'signup'}
                title="CADASTRE-SE"
                onPress={() => this.setState({ selectedTab: 'signup' })}
                selectedTitleStyle={{color: '#ffffff', fontSize: 20, opacity: 1}}
                titleStyle={{color: '#ffffff', fontSize: 20, opacity: 0.5}}>
                <CFormSignup />
            </TabNavigator.Item>
        </TabNavigator>
      )
  }
}

const styles = StyleSheet.create({
    bgStyleCustomHidden: {
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
    removeStyleOfTabs: {
        backgroundColor: 'transparent', 
        marginTop: 55,
        borderWidth: 0,
    },
    removeStyleOfTabBar: {
        position: 'absolute',
        top: 0,
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
})

export { CTabsBar };
