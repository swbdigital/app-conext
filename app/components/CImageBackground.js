import React from 'react';
import { ImageBackground, StatusBar } from 'react-native';

const CImageBackground = ({ children }) => {
    const backgroundImagem = '../images/background-laptop.png';
    return (
        <ImageBackground
            source={require(backgroundImagem)} 
            style={{ flex: 1 }}>
            <StatusBar hidden />
                {children}
        </ImageBackground>
    )
}

export { CImageBackground };
