import React, { Component } from 'react'
import { 
    Container, 
    Content, 
    List, 
    ListItem, 
    Text 
} from 'native-base';

class CSideBar extends Component {
    constructor(props) {
        super(props)
    }    
    state = {
        Listas: [
            {id: 1, name: 'Joao'},
            {id: 2, name: 'Pedro'},
            {id: 3, name: 'Maria'},
            {id: 4, name: 'José'},
            {id: 5, name: 'Euclides'}
        ]
    }

    renderRows() {
        return (
            this.state.Listas.map((itemLista, index) => (
                <ListItem key={index}>
                    <Text>{itemLista.name}</Text>
                </ListItem>
            ))
        )
    }
   
    render() {
        console.log(this.props.navigator);
        return (
            <Container>
                <Content>
                    <List>
                        {this.renderRows()}
                    </List>
                </Content>
            </Container>
        )
    }
}

export { CSideBar }
