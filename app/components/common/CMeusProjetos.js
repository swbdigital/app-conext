import React from 'react'

import { 
  View, 
  Text, 
  StyleSheet, 
  ScrollView, 
  FlatList 
} from 'react-native'

const CMeusProjetos = ({ mprojects, title }) => {
  _keyExtractor = ( item ) => ( item.id.toString() )  
  _renderItem = ({ item }) => (
    <View style={[styles.styleItems, styles.stylingUnderline]}>
      <Text style={{ fontSize: 17 }}>{item.name}</Text>
      <Text style={{ color: '#999999'}}>Causa: {item.cause}</Text>
      <Text>
        <Text style={styles.txtStyleActual}>{item.actual}</Text><Text style={{ color: '#999999'}}>/</Text><Text style={{ color: '#999999'}}>{item.total}</Text> <Text style={{ color: '#999999'}}>{item.type}</Text>
      </Text>
    </View>

  )

  return (
    <ScrollView style={styles.addMargins}>
      <Text style={[styles.txtTitleStyle, styles.containerStyle]}>{title || 'MEUS PROJETOS'}</Text>
      <FlatList 
        data={mprojects}
        renderItem={this._renderItem}
        keyExtractor={this._keyExtractor}
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  txtTitleStyle: {
    color: '#169cf1'
  },
  txtStyleActual: {
    color: '#169cf1',
    fontSize: 20,
  },
  containerStyle: {
    paddingLeft: 15,
    paddingRight: 15
  },
  stylingUnderline: {
    width: '100%', 
    borderTopColor: '#c8c8c8c8', 
    borderTopWidth: 1
  },
  styleItems: {
    width: '100%', 
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderTopColor: '#c8c8c8c8', 
    borderTopWidth: 1
  },
  addMargins: {
    marginTop: 30
  }
})

export { CMeusProjetos }
