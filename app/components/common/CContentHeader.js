import React from 'react'

import { 
  View, 
  Text, 
  ImageBackground 
} from 'react-native';

const CContentHeader = ({ organization }) => {
  const backgroundContentHeader = '../../images/background-laptop.png'; 
  return (
    <View>
      <ImageBackground 
        source={require(backgroundContentHeader)}
        style={{ height: 170 }}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{ color: '#fff', fontSize: 30 }}>Bem-vindo,</Text>
          <Text style={{ color: '#fff', fontSize: 50 }}>{organization}</Text>
        </View>
      </ImageBackground>
    </View>
  )
}

export { CContentHeader }
