import React from 'react'

import { 
  View, 
  Text, 
  ImageBackground 
} from 'react-native';

const CContentHeaderProfile = ({ children }) => {
  const backgroundContentHeader = '../../images/background-laptop.png'; 
  return (
    <View>
      <ImageBackground 
        source={require(backgroundContentHeader)}
        style={{ padding: 20 }}>
        {children}
      </ImageBackground>
    </View>
  )
}

export { CContentHeaderProfile }
