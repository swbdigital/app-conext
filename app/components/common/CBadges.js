import React from 'react'

import {
    ListItem, 
    Text,
    Button
} from 'native-base';

import { 
    Image, 
    FlatList, 
    ScrollView,
    StyleSheet
} from 'react-native'

const badgeIcon = '../../images/medalha.png';
const CBadges = ({ badges }) => {

    return (
        <ScrollView>
            <FlatList
            data={badges}
            renderItem={({ item }) => (
                <ListItem style={{borderBottomWidth: 0}}>
                        <Image source={require(badgeIcon) } style={{ width: 35, height: 35 }} />
                        <Text style={{color: '#000'}}>{ item.name }</Text>
                </ListItem>
            )}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    txtTitleStyle: {
        color: '#169cf1'
      },    
})

export { CBadges }
