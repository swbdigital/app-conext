import React from 'react'

import { 
  View, 
  Text, 
  ImageBackground 
} from 'react-native';

const CContentHeaderSmall = () => {
  const backgroundContentHeader = '../../images/background-laptop.png'; 
  return (
    <View>
      <ImageBackground 
        source={require(backgroundContentHeader)}
        style={{ height: 80 }}>
      </ImageBackground>
    </View>
  )
}

export { CContentHeaderSmall }
