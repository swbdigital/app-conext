import React from 'react'

import { 
  Text, 
  StyleSheet, 
  ScrollView, 
  FlatList,
  Image,
  View
} from 'react-native'

import { Content, List, ListItem, Left, Thumbnail } from 'native-base';


const CAvatarsFollowFollowers = ({ avatars, title }) => {
  _keyExtractor = ( item ) => ( item.id.toString() )
  _renderItem = ({ item }) => (
    <Content>
        <List>
            <ListItem avatar>
              <Left>
                <Thumbnail source={{ uri: item.avatar }} style={{ height: 30, width: 30}}/>
              </Left>
            </ListItem>
          </List>
    </Content>
  )

  return (
    <ScrollView style={styles.addMargins}>
      <Text style={[styles.txtTitleStyle, styles.containerStyle]}>{title}</Text>
        <FlatList 
          data={avatars}
          horizontal={true}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
        />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  txtTitleStyle: {
    color: '#169cf1'
  },
  txtStyleActual: {
    color: '#169cf1',
    fontSize: 20,
  },
  containerStyle: {
    paddingLeft: 15,
    paddingRight: 15
  },
  stylingUnderline: {
    width: '100%', 
    borderTopColor: '#c8c8c8c8', 
    borderTopWidth: 1
  },
  styleItems: {
    width: '100%', 
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderTopColor: '#c8c8c8c8', 
    borderTopWidth: 1,
    flexDirection: 'row',
  },
  addMargins: {
    marginTop: 30
  }
})

export { CAvatarsFollowFollowers }
