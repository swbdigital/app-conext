import React, { Component } from 'react'
import { 
  View, 
  StyleSheet, 
  Image, 
  Text 
} from 'react-native'

import Icon from 'react-native-vector-icons/Entypo';
import {
  Header, 
  Left, 
  Body, 
  Right, 
  Button 
} from 'native-base'


export default class CHeader extends Component {
  constructor(props){
    super(props)

    closeDrawer = this.closeDrawer.bind(this);
    openDrawer = this.openDrawer.bind(this);
  }
  closeDrawer = () => {
    // this.drawer._root.close()
  }

  openDrawer = () => {
    // this.drawer._root.open()
  }
  render() {
    const LogoHeader = '../../images/Logo_min.png'
    return (
          <Header 
            style={styles.containerMenu} 
            androidStatusBarColor='#169cf1'>

            <Left>
              <Button 
                  transparent 
                  onPress={() => this.openDrawer()}>
                <Icon 
                  size={30} 
                  name='menu' 
                  color='#fff' />
              </Button>
            </Left>
            <Body>
              <View 
                style={{ 
                  flexDirection: 'row', 
                  alignItems: 'center' 
                }}>

                  <Image 
                  source={require(LogoHeader)} 
                  style={{ 
                    width: 30, 
                    height: 30, 
                    resizeMode: 'contain'
                  }}/>

                  <Text style={{ 
                    color: '#fff', 
                    marginLeft: 10, 
                    fontWeight: 'bold', 
                    fontSize: 15 }}>{this.props.title || 'CONEXT'}</Text>
              </View>
            </Body>
            <Right>
              <Button transparent>
                  <Icon size={25} name='user' color='#fff' />
              </Button>
            </Right>            
          </Header>
    )
  }
}

const styles = StyleSheet.create({
    containerMenu: {
        backgroundColor: '#169cf1'
    }
})


export { CHeader }
