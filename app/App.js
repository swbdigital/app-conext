import React, { Component } from 'react'
import Routes from './Router';

import SDashboardVoluntarios from './screens/SDashboardVoluntarios';


class App extends Component {
  render() {
    return (
      <Routes />
    )
  }
}

export default App;
