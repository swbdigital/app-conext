import React from 'react'

import LinearGradient from 'react-native-linear-gradient'
import { 
  View,
  StyleSheet, 
  ScrollView, 
  TouchableOpacity,
  Image,
  Text
} from 'react-native'

import { 
  Container,
  Button,
} from 'native-base';

import Icon from 'react-native-vector-icons/MaterialIcons'

import IconFacebook from 'react-native-vector-icons/Entypo'
import IconLinked from 'react-native-vector-icons/AntDesign'
import IconTwitter from 'react-native-vector-icons/Entypo'
import IconGoogle from 'react-native-vector-icons/FontAwesome'

import { 
  CHeader, 
  CContentHeaderProfile,
  CUltimasAvaliacoes,
  CAvatarsFollowFollowers,
  CMeusProjetos,
  CBadges
} from '../components/common'

export default class SPerfilVol extends React.Component {
  state = {
    profile_details: [
      {
        id: 1,
        name: 'Paulo Baier',
        interest: [
          {
            id: 1, 
            name:'Musica'
          }, 
          {
            id:2, 
            name:'Artes'
          }
        ],
        email: 'pb10@cbf.com',
        phone: '(11) 99999-9999',
        image: 'https://www.fillmurray.com/640/360',
        description: `Ea labore eu aliquip officia adipisicing veniam culpa labore aute. Velit excepteur amet occaecat sint irure reprehenderit qui est exercitation. Sit deserunt voluptate sunt ut reprehenderit. Aute cupidatat do ipsum in nisi dolore est ea consectetur.`,
        experience: [
            {
                id: 1,
                name: 'GRAAC',
                cause: 'Projeto Cidadao',
                actual: 5,
                total: 6,
              },
              {
                id: 2,
                name: 'Prouni',
                cause: 'cozinha',
                actual: 1,
                total: 2,
              }
        ],
        follow: [
            {
                id: 1,
                avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
            },
            {
                id: 2,
                avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
            },
            {
                id: 3,
                avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
            }           
        ],
        comments: [
            {
              id: 1,
              name: 'Paulo Baier',
              comment: 'ótima experiencia e aprendizado na instituição',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
            },
            {
              id: 2,
              name: 'Marta',
              comment: 'Consegui desenvolver minhas habilidades :D',
              avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
            },
            {
              id: 3,
              name: 'Julio Cesar',
              comment: 'Faltou um pouco de organização no projeto...',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
            }
        ],        
      }
    ]   
  }

  render() {
    return (
        <Container>
          <ScrollView>
          <CHeader title="Perfil" />

          <CContentHeaderProfile>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Image source={{ uri: this.state.profile_details[0].image }} style={{width: 100, height: 100, borderRadius: 50}} />
                </View>
                <View style={{marginLeft: 20, justifyContent: 'center'}}>
                  <Text style={{color: '#fff'}}>{this.state.profile_details[0].name}</Text>
                  <Text style={{color: '#fff'}}>{this.state.profile_details[0].interest[0].name} | {this.state.profile_details[0].interest[1].name}</Text>
                  <Text style={{color: '#fff'}}>{this.state.profile_details[0].email}</Text>
                  <Text style={{color: '#fff'}}>{this.state.profile_details[0].phone}</Text>
                </View>
              </View>
          </CContentHeaderProfile>

          <View style={{paddingLeft: '5%', paddingRight: '5%', flexDirection: 'row'}}>
            <LinearGradient
                    colors={['#1283f6', '#8811d3']}
                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                    style={styles.containerButton}
                    >
                <TouchableOpacity 
                    onPress={() => {}}>
                    <Text style={styles.buttonText}>CONVIDAR</Text>
                </TouchableOpacity>
            </LinearGradient>      
            <Button bordered dark style={{justifyContent: 'center', marginTop: 20, marginLeft: 10, height: 50, width: 80}}>
                <Text style={styles.buttonText1}>CHAT</Text>
            </Button>                
          </View>
          <View style={{padding: 20, borderBottomWidth: 1, borderBottomColor: '#c8c8c8'}}>
            <Text>{this.state.profile_details[0].description}</Text>
          </View>
          <View>
             <CBadges badges={this.state.profile_details[0].interest} />
          </View>
          <CAvatarsFollowFollowers title="SEGUINDO" avatars={this.state.profile_details[0].follow} />
          <CUltimasAvaliacoes comments={this.state.profile_details[0].comments} title="ÚLTIMAS AVALIAÇÕES" />
          <View style={{backgroundColor: '#e8eaf3', padding: 15}}>
            <Text style={{color: '#666666', marginTop: 5, flexDirection: 'row'}}>REDES SOCIAIS: </Text>
            <View style={{flexDirection: 'row', justifyContent: 'flex-end', marginTop: -20}}>
              <IconFacebook name="facebook-with-circle" size={25} style={{color: '#6f7384'}}/>
              <IconLinked name="linkedin-square" size={25} style={{color: '#6f7384'}} />
              <IconTwitter name="twitter-with-circle" size={25} style={{color: '#6f7384'}}/>
              <IconGoogle name="google-plus-circle" size={25} style={{color: '#6f7384'}} />
            </View>
          </View>
          <View style={{backgroundColor: '#a8abb9', justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{color: '#ffffff', margin: 20}}>Copyright @2018</Text>
          </View>
          </ScrollView>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
  bgStyleCustomHidden: {
    backgroundColor: 'transparent',
},
bgStyleCustomShow: {
    backgroundColor: '#ffffff',
},
paddings: {
    paddingLeft: 7
},
buttonText: {
    color: '#FFF',
    padding: 15,
},
buttonText1: {
    padding: 15,
},
containerButton: {
    width: 100,
    height: 50,
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20
},
recoveryPassword: {
    height: 100,
    justifyContent: 'center'
},
})

