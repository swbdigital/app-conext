import React, { Component } from 'react'

import { 
  View, 
  Text, 
  StyleSheet, 
  ScrollView, 
  TouchableOpacity 
} from 'react-native'

import { 
  Container, 
  Item, 
  Input 
} from 'native-base'

import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/SimpleLineIcons'

import { 
  CHeader, 
  CVolSugeridos, 
  CContentHeader, 
  CMeusProjetos, 
  CUltimosComentarios 
} from '../components/common'

export default class SDashboardOrganizations extends Component {
  state = {
    sugest_volunta: [
      {
        id: 1,
        name: 'Ronaldinho Gaucho',
        start: '10/10/2018',
        actual: 5,
        total: 5,
        type: 'Música'

      },
      {
        id: 2,
        name: 'Ronaldo Fenomeno',
        start: '10/09/2018',
        actual: 5,
        total: 5,
        type: 'Artes'

      },
      {
        id: 3,
        name: 'Kaká',
        start: '22/08/2018',
        actual: 4,
        total: 5,
        type: 'Entusiasmo'

      }      
    ],
    my_projects: [
      {
        id: 1,
        name: 'Escola de futebol',
        cause: 'inclusão social',
        actual: 30,
        total: 50,
        type: 'vagas'
      },
      {
        id: 2,
        name: 'Futebol nas escolas',
        cause: 'esportes',
        actual: 0,
        total: 10,
        type: 'vagas'
      }    
    ],
    comments: [
      {
        id: 1,
        name: 'Paulo Baier',
        comment: 'ótima experiencia e aprendizado na instituição',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
      },
      {
        id: 2,
        name: 'Marta',
        comment: 'Consegui desenvolver minhas habilidades :D',
        avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
      },
      {
        id: 3,
        name: 'Julio Cesar',
        comment: 'Faltou um pouco de organização no projeto...',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
      }
    ]
  }


  constructor(props){
    super(props)
  }
  render() {
    return (
        <Container>
          <ScrollView>
          <CHeader />
          <CContentHeader organization="CBF"/>
          <View
            style={{ 
              height: 150, 
              justifyContent: 'center',
              paddingLeft: '10%',
              paddingRight: '10%'
            }}>

            <Item rounded style={{ paddingLeft: 10, paddingRight: 10, height: 40 }}>
              <View>
                <Icon name="magnifier"/>
              </View>
              <Input placeholder='Pesquisar' />
              <View>
                <Icon name="equalizer" style={{transform: [{ rotate: '-90deg' }]}}/>
              </View>
            </Item>

          </View>
          <View style={{ minHeight: 200 }}>
            <CVolSugeridos sugest={this.state.sugest_volunta} title="VOLUNTARIOS SUGERIDOS"/>
            <CMeusProjetos mprojects={this.state.my_projects} />
            <CUltimosComentarios comments={this.state.comments} title="ÚLTIMOS COMENTÁRIOS" />
          </View>
          <View>
          <LinearGradient
                  colors={['#1283f6', '#8811d3']}
                  start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                  style={styles.containerButton}
                  >
              <TouchableOpacity 
                  onPress={() => {}}>
                  <Text style={styles.buttonText}>ADICIONAR PROJETO</Text>
              </TouchableOpacity>
          </LinearGradient>            
          </View>
          </ScrollView>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
  styleContainerView: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonText: {
    color: '#FFF',
    padding: 15,
  },
  containerButton: {
      width: '90%',
      height: 50,
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 30,
      marginBottom: 30,
  },
  containerMenu: {
    backgroundColor: '#169cf1'
  }  
})

