import React from 'react'

import { 
  View, 
  Dimensions,
  Image, 
  Text,
  StyleSheet
} from 'react-native'

import { CImageBackground, CTabsBar } from '../components'


const logoImage = '../images/Logo.png';

export default SAppAccess = () => (
    <CImageBackground>
      <View style={styles.container}>
        <View style={styles.containerLogo}>
        <Image source={ require(logoImage) } style={styles.logoStyle} />
      </View>
        <View style={styles.containerTitle}>
        <Text style={styles.titleSmall}>Bem-vindo ao,</Text>
        <Text style={styles.titleBig}>CONEXT</Text>
      </View>
        <View style={styles.containerAuth}>
          <CTabsBar style={styles.backCustomStyle} />
      </View>
      </View>
    </CImageBackground>
)

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  containerLogo: {
    flex: 1,
    width: width,
    height: 25,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'flex-end',
  },
  logoStyle: {
    width: 90,
    height: 90
  },
  containerTitle: {
    flex: 1,
    width: width,
    height: 25,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
  },
  titleSmall: {
    fontSize: 30,
    color: '#fff'
  },
  titleBig: {
    fontSize: 50,
    color: '#fff',
    fontWeight: 'bold',
  },
  containerAuth: {
    flex: 2,
    width: width,
    paddingLeft: 20,
    paddingRight: 20,
  },
  backCustomStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  }
})
