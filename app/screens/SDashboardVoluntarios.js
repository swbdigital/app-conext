import React from 'react'

import LinearGradient from 'react-native-linear-gradient'
import { 
  View, 
  Text, 
  StyleSheet, 
  ScrollView, 
  TouchableOpacity 
} from 'react-native'

import { 
  Container, 
  Item, 
  Input 
} from 'native-base'

import Icon from 'react-native-vector-icons/SimpleLineIcons'

import { 
  CMeusProjetos, 
  CHeader, 
  CContentHeader, 
  COrgSugeridos, 
  CUltimosComentarios 
} from '../components/common'

export default class SDashboardVoluntarios extends React.Component {
  state = {
    sugest_volunta: [
      {
        id: 1,
        name: 'AACD',
        theme: 'Projeto Música para todos',
        actual: 100,
        total: 200,
        type: 'vagas'
      },
      {
        id: 2,
        name: 'Coca-Cola',
        theme: 'Projeto de solidariedade',
        actual: 50,
        total: 60,
        type: 'vagas'
      },
      {
        id: 3,
        name: 'Orfanato',
        theme: 'Aulas de desenho',
        actual: 0,
        total: 1,
        type: 'vagas'
      }      
    ],
    my_projects: [
      {
        id: 1,
        name: 'GRAAC',
        cause: 'Projeto Cidadao',
        actual: 5,
        total: 6,
        type: 'vagas'
      },
      {
        id: 2,
        name: 'Prouni',
        cause: 'cozinha',
        actual: 1,
        total: 2,
        type: 'vagas'
      }    
    ],
    comments: [
      {
        id: 1,
        name: 'Paulo Baier',
        comment: 'ótima experiencia e aprendizado na instituição',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
      },
      {
        id: 2,
        name: 'Marta',
        comment: 'Consegui desenvolver minhas habilidades :D',
        avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
      },
      {
        id: 3,
        name: 'Julio Cesar',
        comment: 'Faltou um pouco de organização no projeto...',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
      }
    ]
  }

  render() {
    return (
        <Container>
          <ScrollView>
          <CHeader />
          <CContentHeader organization="ALEX HUNTER"/>
          <View
            style={{ 
              height: 150, 
              justifyContent: 'center',
              paddingLeft: '10%',
              paddingRight: '10%'
            }}>

            <Item rounded style={{ paddingLeft: 10, paddingRight: 10, height: 40 }}>
              <View>
                <Icon name="magnifier"/>
              </View>
              <Input placeholder='Pesquisar' />
              <View>
                <Icon name="equalizer" style={{transform: [{ rotate: '-90deg' }]}}/>
              </View>
            </Item>

          </View>
          <View style={{ minHeight: 200 }}>
            <COrgSugeridos sugest={this.state.sugest_volunta} title="ORGANIZAÇÕES SUGERIDAS" />
            <CMeusProjetos mprojects={this.state.my_projects} title="MEUS PROJETOS" />
            <CUltimosComentarios comments={this.state.comments} title="SEGUINDO" />
          </View>
          <View>
          <LinearGradient
                  colors={['#1283f6', '#8811d3']}
                  start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                  style={styles.containerButton}
                  >
              <TouchableOpacity 
                  onPress={() => {}}>
                  <Text style={styles.buttonText}>EDITAR PERFIL</Text>
              </TouchableOpacity>
          </LinearGradient>            
          </View>
          </ScrollView>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
  styleContainerView: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonText: {
    color: '#FFF',
    padding: 15,
  },
  containerButton: {
      width: '90%',
      height: 50,
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 30,
      marginBottom: 30,
  },
  containerMenu: {
    backgroundColor: '#169cf1'
  }  
})

