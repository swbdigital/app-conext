import React from 'react'

import LinearGradient from 'react-native-linear-gradient'
import { 
  View,
  StyleSheet, 
  ScrollView, 
  TouchableOpacity,
  Image,
  Text
} from 'react-native'

import { 
  Container,
  Content,
  Card,
  CardItem,
  // Text,
  Button,
  Left,
  Body,
  Right 
} from 'native-base';

import Icon from 'react-native-vector-icons/MaterialIcons'

import { 
  CHeader, 
  CContentHeaderSmall,
  CUltimasAvaliacoes,
  CAvatarsFollowFollowers
} from '../components/common'

export default class SDashboardVoluntarios extends React.Component {
  state = {
    sugest_volunta: [
      {
        id: 1,
        name: 'AACD',
        theme: 'Projeto Música para todos',
        actual: 100,
        total: 200,
        type: 'vagas'
      },
      {
        id: 2,
        name: 'Coca-Cola',
        theme: 'Projeto de solidariedade',
        actual: 50,
        total: 60,
        type: 'vagas'
      },
      {
        id: 3,
        name: 'Orfanato',
        theme: 'Aulas de desenho',
        actual: 0,
        total: 1,
        type: 'vagas'
      }      
    ],
    my_projects: [
      {
        id: 1,
        name: 'GRAAC',
        cause: 'Projeto Cidadao',
        actual: 5,
        total: 6,
        type: 'vagas'
      },
      {
        id: 2,
        name: 'Prouni',
        cause: 'cozinha',
        actual: 1,
        total: 2,
        type: 'vagas'
      }    
    ],
    comments: [
      {
        id: 1,
        name: 'Paulo Baier',
        comment: 'ótima experiencia e aprendizado na instituição',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
      },
      {
        id: 2,
        name: 'Marta',
        comment: 'Consegui desenvolver minhas habilidades :D',
        avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
      },
      {
        id: 3,
        name: 'Julio Cesar',
        comment: 'Faltou um pouco de organização no projeto...',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
      }
    ],
    avatars: [
      {
        id: 1,
        avatar: 'https://randomuser.me/api/portraits/thumb/men/19.jpg'
      },
      {
        id: 2,
        avatar: 'https://randomuser.me/api/portraits/thumb/women/87.jpg'
      },
      {
        id: 3,
        avatar: 'https://randomuser.me/api/portraits/thumb/men/39.jpg'
      }
    ]    
  }

  render() {
    const badgeIcon = '../images/medalha.png';
    return (
        <Container>
          <ScrollView>
          <CHeader title="Detalhes do Projeto" />
          <CContentHeaderSmall />
          <View
            style={{ 
              position: 'relative',
              top: -42,
              justifyContent: 'center',
              paddingLeft: '3%',
              paddingRight: '3%'
            }}>

            <Content style={{borderRadius: 25}}>
              <Card >
                <CardItem style={{borderBottomColor: '#aaa', borderBottomWidth: 1}}>
                  <Left>
                    <Body>
                      <Text style={{ fontSize: 20 }}>AACD</Text>
                      <Text style={{ color: '#999999'}}>Projeto Música para todos</Text>
                      <Text>
                        <Text style={styles.txtStyleActual}>100</Text><Text style={{ color: '#999999'}}>/</Text><Text style={{ color: '#999999'}}>200</Text> <Text style={{ color: '#999999'}}>vagas</Text>
                      </Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image style={{height: 200, width: null, flex: 1}}/>
                </CardItem>
                <CardItem style={{borderTopColor: '#aaa', borderTopWidth: 1}}>
                  <Left style={{marginRight: -24}}>
                    <Button transparent>
                      <Image source={require(badgeIcon) } style={{ width: 35, height: 35 }} />
                      <Text style={{color: '#000'}}>Música</Text>
                    </Button>
                  </Left>
                  <Body style={{marginRight: -15}}>
                    <Button transparent>
                      <Icon active name="star" size={25} style={{color: '#ffc000'}} />
                      <Text>Deficiências</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button transparent>
                      <Icon active name="access-time" size={25} style={{marginRight: 5, color: '#afafaf'}}/>
                      <Text>2000 hrs</Text>
                    </Button>                    
                  </Right>
                </CardItem>
              </Card>
            </Content>            

          </View>
          <View style={{paddingLeft: '5%', paddingRight: '5%'}}>
            <Text style={{color: '#169cf1', marginBottom: 12}}>DESCRIÇÃO</Text>
            <Text>Exercitation fugiat in cillum mollit ut et aliquip 
                  aliquip ullamco fugiat est adipisicing occaecat commodo. 
                  Cupidatat labore exercitation eiusmod deserunt irure ex. 
                  Fugiat consectetur in mollit officia quis ea enim.
            </Text>
          </View>
          <CAvatarsFollowFollowers title="VOLUNTARIOS" avatars={this.state.avatars} />
          <CUltimasAvaliacoes comments={this.state.comments} title="ÚLTIMAS AVALIAÇÕES" />
          <View>
          <LinearGradient
                  colors={['#1283f6', '#8811d3']}
                  start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                  style={styles.containerButton}
                  >
              <TouchableOpacity 
                  onPress={() => {}}>
                  <Text style={styles.buttonText}>INSCREVER-SE</Text>
              </TouchableOpacity>
          </LinearGradient>            
          </View>
          </ScrollView>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
  styleContainerView: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonText: {
    color: '#FFF',
    padding: 15,
  },
  containerButton: {
      width: '90%',
      height: 50,
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 30,
      marginBottom: 30,
  },
  containerMenu: {
    backgroundColor: '#169cf1'
  },
  txtTitleStyle: {
    color: '#169cf1'
  },
  txtStyleActual: {
    color: '#169cf1',
    fontSize: 20,
  },
  containerStyle: {
    paddingLeft: 15,
    paddingRight: 15
  },
  stylingUnderline: {
    width: '100%', 
    borderTopColor: '#c8c8c8c8', 
    borderTopWidth: 1
  },
  styleItems: {
    width: '100%', 
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 4,
    paddingBottom: 4
  }  
})

