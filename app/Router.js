import React from 'react';
import { Scene, Router } from 'react-native-router-flux';

import SAppAccess from './screens/SAppAccess'
import SDashboardOrganizations from './screens/SDashboardOrganizations'
import SDashboardVoluntarios from './screens/SDashboardVoluntarios'
import SDetailsOfProject from './screens/SDetailsOfProject'
import SPerfilVol from './screens/SPerfilVol'
import SPerfilOrg from './screens/SPerfilOrg'


const RouterComponent = () => {
    return (
        <Router>
            <Scene key="root" hideNavBar>
                {/* <Scene key="auth">
                    <Scene 
                        key="appaccess" 
                        component={SAppAccess} 
                        hideNavBar 
                        initial
                    />
                </Scene> */}
                {/* <Scene key="organizations"> */}
                    {/* <Scene 
                        key="dashboardOrganizations"
                        component={SDashboardOrganizations}
                        hideNavBar
                        initial
                    /> */}
                    <Scene 
                        key="detailsPerfilOrg"
                        component={SPerfilOrg}
                        hideNavBar
                    />
                {/* </Scene> */}
                {/* <Scene key="voluntarios"> */}
                    {/* <Scene 
                        key="dashboardVoluntarios"
                        component={SDashboardVoluntarios}
                        hideNavBar
                        initial
                    /> */}
                    {/* <Scene 
                        key="detailsPerfilVol"
                        component={SPerfilVol}
                        hideNavBar
                    /> */}
                {/* </Scene> */}
                {/* <Scene key="detailsProject">
                    <Scene 
                        key="details"
                        component={SDetailsOfProject}
                        hideNavBar
                        initial
                    />
                </Scene> */}
            </Scene>
        </Router>
    );
};

export default RouterComponent;